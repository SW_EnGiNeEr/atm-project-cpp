When the program is run through the CodeBlocks IDE, the user will enter the pin "1005". The user gets 3 attempts to enter in the correct pin. If the wrong pin is entered, the program will tell the user that they only have 2 attempts left. When the user has entered the pin incorrectly 3 times, the program will terminate with a warning message to the user. 

If the user successfully enters the pin, the user will then enter numbers on the keypad that correspond to the choice that the user wants to make.

From here, the program is self-explanitory and the ATM software will guide the user through the use of the system. 

 